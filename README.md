# SpringBatch_101

I found a tutorial that starts with the SpringBoot Initializr. So, I went ahead and started this project with that.
But I wanted it to be version controlled on GitLab and I want to use IntelliJ, so I did the following...

    Initialize the project:
	• Open the Initializr web site… 
	• selected the Spring Batch dependency 
	• generated a project called spring-batch-hello-world (artifact: spring-batch-helloworld) 
	• Downloaded the resulting ZIP file to $PROJECTS\SpringInitializr (file: spring-batch-helloworld.zip)
	• Extracted the zip file to a new folder in the same directory…  $PROJECTS\SpringInitializr\spring-batch-helloworld
	
	Created the GitLab Prpoject
	• Went to the GitLab web site and created a new project SpringBatch_101
	• Cloned the GitLab project to the $PROJECTS directory, which created a new subdirectory: $PROJECTS\springbatch_101
	• Copied Initializr-generated files from $PROJECTS\SpringInitializr\spring-batch-helloworld to $PROJECTS\springbatch_101
	• $PROJECTS\springbatch_101> git add *
	• $PROJECTS\springbatch_101> git commit -m "initial commit"
	• $PROJECTS\springbatch_101> git push origin master
	• Delete the $PROJECTS\springbatch_101 directory (all the files are in GitLab now)
	
	Set up the IDE:
	• Open IntelliJ…
	• Get project from GitLab repository and create a working directory at  $PROJECTS creating $PROJECTS\springbatch_101
	
Tutorial = https://codenotfound.com/spring-batch-example.html
